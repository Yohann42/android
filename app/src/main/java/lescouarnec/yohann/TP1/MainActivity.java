package lescouarnec.yohann.TP1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.TP1.R;

public class MainActivity extends AppCompatActivity {
    EditText AcceuilNom;
    Button AcceuilConfirmer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AcceuilConfirmer = this.findViewById(R.id.AccueilButton);
        AcceuilNom = this.findViewById(R.id.AccueilNom);

        AcceuilConfirmer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(AcceuilNom != null) {
                    Intent intent = new Intent(MainActivity.this, Activity2.class);
                    intent.putExtra("Nom", AcceuilNom.getText().toString());
                    MainActivity.this.startActivity(intent);
                }
            }});

    }


}
