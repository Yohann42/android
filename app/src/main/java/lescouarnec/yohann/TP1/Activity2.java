package lescouarnec.yohann.TP1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.TP1.R;

public class Activity2 extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2);

        Intent intent = getIntent();
        TextView welcome = this.findViewById(R.id.WelcomeMessage);
        welcome.setText("Bienvenue\n" + intent.getStringExtra("Nom"));
    }
}
